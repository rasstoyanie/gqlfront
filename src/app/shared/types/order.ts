import {User} from '../../core/types/user';
import {Product} from '../../product/types/product';

export class Order {
  id: number;
  number: string;
  user: User;
  products: Product[];
  timestamp: number;
}
