import gql from 'graphql-tag';

export const messageFragment = gql`
  fragment MessageResponse on Message {
    text
    type
  }
`;
