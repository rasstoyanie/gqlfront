import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from './core/services/user.service';
import {User} from './core/types/user';
import {Subscription} from 'rxjs/Subscription';
import {LoginFormComponent} from './authentication/login-form/login-form.component';
import {Modal} from './core/types/modal';
import {ModalService} from './core/services/modal.service';
import {ModalOptions} from './core/types/modal-options';
import {AuthenticationService} from './core/services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  title = 'app';
  user: User;

  constructor(
    private userService: UserService,
    private modalService: ModalService,
    private authService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.subscription = this.userService.authenticated.subscribe(authenticated => {
      if (authenticated === false) {
        this.user = null;
        this.modalService.openModal(
          new Modal(LoginFormComponent, new ModalOptions('Login Form', true, true, true))
        );
      } else
        this.user = this.userService.getUser();
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  logout() {
    this.authService.logout();
  }
}
