import {Component, OnDestroy, OnInit} from '@angular/core';

import {ProductService} from '../product.service';
import {Product} from '../types/product';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {ClrDatagridStateInterface} from '@clr/angular';

@Component({
  selector: 'app-product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.css']
})
export class ProductGridComponent implements OnInit, OnDestroy {

  products: Product[] = [];
  selectedProducts: Product[] = [];
  subscription: Subscription;
  loading = true;

  constructor(private productService: ProductService) { }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  refresh(datagridState: ClrDatagridStateInterface) {
    this.loading = true;
    this.subscription = this.productService.getProducts(datagridState).subscribe(products => {
      this.products = products;
      this.loading = false;
    });
  }
}
