import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ProductGridComponent} from './product-grid/product-grid.component';
import {ProductViewComponent} from './product-view/product-view.component';

const productRoutes: Routes = [
  { path: 'products', component: ProductGridComponent, pathMatch: 'full' },
  { path: 'products/:d', component: ProductViewComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class ProductRoutingModule { }
