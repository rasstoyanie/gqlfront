import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ClarityModule} from '@clr/angular';

import { ProductGridComponent } from './product-grid/product-grid.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { ProductCreateFormComponent } from './product-create-form/product-create-form.component';
import { ProductUpdateFormComponent } from './product-update-form/product-update-form.component';
import {ProductRoutingModule} from './product-routing.module';
import {ProductService} from './product.service';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    ProductRoutingModule
  ],
  exports: [
    ProductGridComponent,
    ProductViewComponent,
    ProductCreateFormComponent,
    ProductUpdateFormComponent
  ],
  declarations: [
    ProductGridComponent,
    ProductViewComponent,
    ProductCreateFormComponent,
    ProductUpdateFormComponent
  ],
  providers: [
    ProductService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ProductModule { }
