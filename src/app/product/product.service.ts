import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {productListQuery, ProductListQueryResponse} from './graphql/query/productListQuery';
import {Observable} from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import {productByIdQuery, ProductByIdQueryResponse} from './graphql/query/productByIdQuery';
import {environment} from '../../environments/environment';
import {Product} from './types/product';
import {ClrDatagridStateInterface} from '@clr/angular';

@Injectable()
export class ProductService {

  constructor(private apollo: Apollo) { }


  getProducts(datagridState: ClrDatagridStateInterface): Observable<Product[]> {
    return this.apollo.query<ProductListQueryResponse>({
      query: productListQuery,
      variables: { datagridState: datagridState },
      context: { uri: environment.apiUrl + 'product'}
    }).pipe(
      map(({data}) => data.productList)
    );
  }
  getProductById(id: number): Observable<Product> {
    return this.apollo.query<ProductByIdQueryResponse>({
      query: productByIdQuery,
      variables: { id: id },
      context: { uri: environment.apiUrl + 'product'}
    }).pipe(
      map(({data}) => data.productById)
    );
  }
}
