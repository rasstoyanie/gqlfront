import gql from 'graphql-tag';
import {Product} from '../../types/product';

export const productByIdQuery = gql`
  query productByIdQuery ($id: String!) {
    productById (id: $id) {
      id
      name
      price
    }
  }
`;

export interface ProductByIdQueryResponse {
  productById: Product;
}
