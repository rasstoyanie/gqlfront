import gql from 'graphql-tag';
import {Product} from '../../types/product';
import {DatagridState} from '../../../shared/types/datagrid-state';

export const productListQuery = gql`
  query productListQuery ($datagridState: DatagridState!) {
    productList (datagridState: $datagridState) {
      id
      name
      price
    }
  }
`;

export interface ProductListQueryResponse {
  productList: Product[];
}
