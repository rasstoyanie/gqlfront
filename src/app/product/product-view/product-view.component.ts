import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ProductService} from '../product.service';
import {Product} from '../types/product';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  product: Observable<Product>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.product = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.productService.getProductById(Number(params.get('id')))
      )
    );
  }

}
