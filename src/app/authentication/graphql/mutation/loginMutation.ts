import gql from 'graphql-tag';
import {User} from '../../../core/types/user';
import {LoginForm} from '../../types/login-form';

export const loginMutation = gql`
  mutation loginMutation($loginForm: LoginForm!) {
    login(loginForm: $loginForm) {
      user {
        id
        username
        email
      }
    }
  }
`;

export interface LoginMutationResponse {
  login: {
    user: User,
  };
}
export interface LoginMutationVariables {
  loginForm: LoginForm;
}
