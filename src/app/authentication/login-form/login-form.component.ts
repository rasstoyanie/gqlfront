import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Location } from '@angular/common';
import {AuthenticationService} from '../../core/services/authentication.service';
import {LoginForm} from '../types/login-form';
import {User} from '../../core/types/user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ValidatorService} from '../../core/services/validator.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @ViewChild('loginFormElement') loginFormElement: ElementRef;

  loginFormModel = new LoginForm;
  loginForm: FormGroup;
  user: User;

  constructor(private authService: AuthenticationService, private validator: ValidatorService, private location: Location) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(this.loginFormModel.username, Validators.required),
      password: new FormControl(this.loginFormModel.password, Validators.required),
    }, {
      updateOn: 'blur',
      // asyncValidators: this.validator.validateForm(this.loginFormElement.nativeElement.getAttribute('id'))
    });
  }

  onSubmit() {
    this.loginFormModel = this.loginForm.value;
    this.authService.login(this.loginFormModel).subscribe(user => {
      this.user = user;
    });
  }
}
