import {NgModule, OnInit} from '@angular/core';
import {Apollo, ApolloModule} from 'apollo-angular';
import {HttpLink, HttpLinkModule} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '../../environments/environment';

@NgModule({
  imports: [],
  exports: [
    HttpClientModule,
    ApolloModule,
    HttpLinkModule
  ],
  declarations: [],
  providers: []
})
export class GraphQlModule implements OnInit {

  constructor(
    private apollo: Apollo,
    private httpLink: HttpLink
  ) {
    const link = httpLink.create({
      uri: environment.apiUrl,
      withCredentials: true
    });

    apollo.create({
      link: link,
      cache: new InMemoryCache()
    });
  }

  ngOnInit() { }
}
