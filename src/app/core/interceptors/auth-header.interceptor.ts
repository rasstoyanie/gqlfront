import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TokenService} from '../services/token.service';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const newRequest = request.clone({
      setHeaders: this.tokenService.getAuthHeader()
    });
    return next.handle(newRequest);
  }
}
