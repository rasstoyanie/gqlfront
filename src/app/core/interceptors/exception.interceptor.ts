import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {ExceptionService} from '../services/exception.service';
import {Exception} from '../types/exception';

@Injectable()
export class ExceptionInterceptor implements HttpInterceptor {

  constructor(private exceptionService: ExceptionService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (error: any) => {
      if (error instanceof HttpErrorResponse && error.status !== 401) {
        this.exceptionService.setException(new Exception(error.error.message));
      }
    });
  }
}
