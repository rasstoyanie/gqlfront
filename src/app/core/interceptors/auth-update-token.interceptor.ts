import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TokenService} from '../services/token.service';
import {AuthenticationService} from '../services/authentication.service';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthUpdateTokenInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (event.body.extensions && event.body.extensions.token)
          this.tokenService.updateToken(event.body.extensions.token);
      }
    });
  }
}
