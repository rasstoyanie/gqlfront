import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {TokenService} from '../services/token.service';
import {AuthenticationService} from '../services/authentication.service';
import 'rxjs/add/operator/do';
import {ModalService} from '../services/modal.service';
import {LoginFormComponent} from '../../authentication/login-form/login-form.component';
import {Modal} from '../types/modal';

@Injectable()
export class AuthExceptionInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (error: any) => {
      if (error instanceof HttpErrorResponse) {
        if (error.status === 401)
          this.authService.logout();
      }
    });
  }
}
