import {Injectable, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {Apollo} from 'apollo-angular';
import {Observable} from 'rxjs/Observable';
import {User} from '../types/user';
import {loginMutation, LoginMutationResponse, LoginMutationVariables} from '../../authentication/graphql/mutation/loginMutation';
import {LoginForm} from '../../authentication/types/login-form';
import {environment} from '../../../environments/environment';
import {TokenService} from './token.service';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AuthenticationService {

  private loginEventSource = new Subject<boolean>();
  loginEvent = this.loginEventSource.asObservable();

  constructor(
    private apollo: Apollo,
    private tokenService: TokenService,
  ) { }

  login(loginForm: LoginForm): Observable<User> {
    return this.apollo.mutate<LoginMutationResponse, LoginMutationVariables>({
      mutation: loginMutation,
      variables: { loginForm: loginForm },
      context: { uri: environment.apiUrl + 'login'}
    }).pipe(
      map((json) => {
        const user = this.tokenService.setToken(json.data.login.user, json.extensions.token);
        this.loginEventSource.next(true);
        return user;
      })
    );
  }

  logout(): void {
    this.tokenService.removeToken();
    this.loginEventSource.next(false);
  }
}
