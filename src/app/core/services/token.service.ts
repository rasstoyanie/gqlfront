import { Injectable } from '@angular/core';
import {User} from '../types/user';

@Injectable()
export class TokenService {

  constructor() { }

  setToken(user: User, token?: string): User {
    user.token = token ? token : user.token;
    localStorage.setItem('currentUser', JSON.stringify({...user}));
    return user;
  }

  updateToken(token: string): User | boolean {
    const currentUser: User = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      currentUser.token = token;
      localStorage.setItem('currentUser', JSON.stringify(currentUser));
      return currentUser;
    } else {
      return false;
    }
  }

  getToken(): string {
    const currentUser = localStorage.getItem('currentUser');
    if (currentUser !== null) {
      const user: User = JSON.parse(currentUser);
      return user.token;
    }
  }

  removeToken(): void {
    localStorage.removeItem('currentUser');
  }

  getAuthHeader(): {} {
    const token: string = this.getToken();
    if (token)
      return { 'Authorization': 'Bearer ' + token };
  }

  hasToken(): boolean {
    return !(!this.getToken());
  }
}
