import {Injectable} from '@angular/core';
import {Modal} from '../types/modal';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class ModalService {

  private modalSource = new ReplaySubject<Modal>();
  modal = this.modalSource.asObservable();

  constructor() { }

  openModal(modal: Modal) {
    this.modalSource.next(modal);
  }
}
