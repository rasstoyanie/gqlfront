import {Injectable} from '@angular/core';
import {Exception} from '../types/exception';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class ExceptionService {

  private exceptionSource = new ReplaySubject<Exception>();
  exception = this.exceptionSource.asObservable();

  constructor() { }

  setException(exception: Exception) {
    this.exceptionSource.next(exception);
  }
}
