import { Injectable } from '@angular/core';
import {Apollo} from 'apollo-angular';
import {environment} from '../../../environments/environment';
import {debounceTime, map} from 'rxjs/operators';
import {AsyncValidatorFn, FormGroup} from '@angular/forms';
import {validatorQuery, ValidatorQueryResponse, ValidatorVariables} from '../graphql/query/validatorQuery';
import {Observable} from 'rxjs/Observable';
import {Violation} from '../types/violation';
import {Validator} from '../types/validator';

@Injectable()
export class ValidatorService {

  constructor(private apollo: Apollo) { }

  validateForm (formName: string): AsyncValidatorFn {
    return (form: FormGroup): Observable<Violation[]> => {
      return this.apollo.query<ValidatorQueryResponse, ValidatorVariables>({
        query: validatorQuery,
        variables: {validator: new Validator(formName, form)},
        context: {uri: environment.apiUrl + 'validator'}
      }).pipe(
        map(({data}) => data.violations)
      );
    };
  }
}
