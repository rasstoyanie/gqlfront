import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {Subscription} from 'rxjs/Subscription';
import {User} from '../types/user';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {TokenService} from './token.service';

@Injectable()
export class UserService {
  subscription: Subscription;

  private authenticatedSource = new BehaviorSubject<boolean>(false);
  authenticated = this.authenticatedSource.asObservable();

  constructor(
    private authService: AuthenticationService,
    private tokenService: TokenService,
  ) {
    this.authenticatedSource.next(this.tokenService.hasToken());
    this.subscription = this.authService.loginEvent.subscribe(login => {
      this.authenticatedSource.next(login);
    });
  }

  getUser(): User {
    const currentUser = localStorage.getItem('currentUser');
    if (currentUser !== null)
      return JSON.parse(currentUser);
  }

  setUser() {

  }
}
