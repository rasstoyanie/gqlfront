import {NgModule, Optional, SkipSelf} from '@angular/core';
import {ClarityModule} from '@clr/angular';
import {GraphQlModule} from './graph-ql.module';
import {throwIfAlreadyLoaded} from './core-guard';
import {ValidatorService} from './services/validator.service';
import {ExceptionComponent} from './components/exception/exception.component';
import {ExceptionService} from './services/exception.service';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ExceptionInterceptor} from './interceptors/exception.interceptor';
import {AuthExceptionInterceptor} from './interceptors/auth-exception.interceptor';
import {AuthHeaderInterceptor} from './interceptors/auth-header.interceptor';
import {AuthUpdateTokenInterceptor} from './interceptors/auth-update-token.interceptor';
import {TokenService} from './services/token.service';
import {AuthenticationModule} from '../authentication/authentication.module';
import {AuthenticationService} from './services/authentication.service';
import {ModalService} from './services/modal.service';
import {ModalComponent} from './components/modal/modal.component';
import {ModalDirective} from './directives/modal.directive';
import {UserService} from './services/user.service';

@NgModule({
  imports: [
    ClarityModule,
    CommonModule,
    AuthenticationModule
  ],
  exports: [
    ClarityModule,
    GraphQlModule,
    CommonModule,
    AuthenticationModule,
    ExceptionComponent,
    ModalComponent
  ],
  declarations: [ExceptionComponent, ModalDirective, ModalComponent],
  providers: [
    ExceptionService,
    ModalService,
    TokenService,
    UserService,
    AuthenticationService,
    ValidatorService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthHeaderInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthUpdateTokenInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ExceptionInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthExceptionInterceptor, multi: true},
  ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
