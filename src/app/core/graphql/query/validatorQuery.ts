import gql from 'graphql-tag';
import {Violation} from '../../types/violation';
import {Validator} from '../../types/validator';

export const validatorQuery = gql`
  query validatorQuery ($validator: Validator!) {
    violations (validator: $validator) {
      field
      errors
    }
  }
`;

export interface ValidatorQueryResponse {
  violations: Violation[];
}

export interface ValidatorVariables {
  validator: Validator;
}
