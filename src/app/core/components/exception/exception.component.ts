import {Component, OnDestroy, OnInit} from '@angular/core';
import {Exception} from '../../types/exception';
import {ExceptionService} from '../../services/exception.service';
import {filter} from 'rxjs/operators';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-exception',
  templateUrl: './exception.component.html',
  styleUrls: ['./exception.component.css']
})
export class ExceptionComponent implements OnInit, OnDestroy {

  exceptions: Exception[] = [];
  subscription: Subscription;

  constructor(private exceptionService: ExceptionService) { }

  ngOnInit() {
    this.subscription = this.exceptionService.exception.subscribe(exception => {
      this.exceptions.unshift(exception);
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  remove(exception: Exception) {
    this.exceptions.splice(this.exceptions.indexOf(exception), 1);
  }
}
