import {Component, OnDestroy, OnInit} from '@angular/core';
import {Modal} from '../../types/modal';
import {Subscription} from 'rxjs/Subscription';
import {ModalService} from '../../services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, OnDestroy {
  modals: Modal[] = [];
  subscription: Subscription;

  constructor(private modalService: ModalService) { }

  ngOnInit() {
    this.subscription = this.modalService.modal.subscribe(modal => {
      const singleExists = this.modals.find(function (item) {
        return item.component === modal.component && item.options.single === true;
      });
      if (!singleExists)
        this.modals.push(modal);
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onOpenChange(modal: Modal, value: boolean) {
    if (value === false)
      this.modals.splice(this.modals.indexOf(modal), 1);
  }
}
