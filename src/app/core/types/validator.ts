import {FormGroup} from '@angular/forms';

export class Validator {
  form: string;
  fields: any;

  constructor (formName: string, form: FormGroup) {
    this.form = formName;
    this.fields = form.value;
  }
}
