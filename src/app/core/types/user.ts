export class User {
  id: string;
  username: string;
  private _name?: string;
  email: string;
  token?: string;
  roles?: string[];

  get name(): string {
    return this._name || this.username;
  }

  set name(value: string) {
    this._name = value;
  }
}
