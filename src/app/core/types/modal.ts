import {Type} from '@angular/core';
import {ModalOptions} from './modal-options';

export class Modal {
  opened = false;
  constructor(public component: Type<any>, public options: ModalOptions, public data?: any) {
    this.opened = options.opened;
  }

  open() {
    this.opened = true;
  }

  close() {
    this.opened = false;
  }
}
