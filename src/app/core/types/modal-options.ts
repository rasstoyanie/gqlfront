
export class ModalOptions {
  constructor(public title: string, public opened = true, public closable = true, public single = false) { }
}
