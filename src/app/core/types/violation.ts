import {ValidationErrors} from '@angular/forms';

export class Violation {
  field: string;
  errors: ValidationErrors[];
}
