export class Exception {
  message: string;
  active = true;
  constructor(message: string) {
    this.message = message;
  }
}
